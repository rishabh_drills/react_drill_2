import React from 'react';
import countries from './countries.json'
// import logo from './logo.svg';
import Country from './countries'
// import './App.css';
import Dropdown from './dropdown'

class App extends React.Component {
  state = {
    Country: countries
  }

  handleFilterInput = (event) => {
    let value = event.toLowerCase();
    let filtered = countries.filter((obj) => {
      if(obj.region.toLowerCase().includes(value)){
        return (obj);
      }
    })
    this.setState({Country: filtered})
    
  }

  render() { 
    return <div style={{display: 'flex', justifyContent: 'center', flexDirection: 'column', padding: '2rem'}}>
      <Dropdown changes={this.handleFilterInput}/>
    <div style={{display:'flex', flexWrap:'wrap', justifyContent: 'center'}}>
      {this.state.Country.map((country) => {
      return <Country key={country["cca2"]} {...country}/>
    })} ;
    </div>  
    </div>    
  }
}
 
export default App;
