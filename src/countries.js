import React from 'react';

const Country = (props) => {
    // console.log(props)
    return <div style={{width: '30%', padding: '3rem', color:'white',backgroundColor: 'grey', margin: '1rem', textAlign:'center'}}>
        <p>{JSON.stringify(props.name.common)}</p>         
        <p>{JSON.stringify(props.region)}</p>
        <p>{JSON.stringify(props.subregion)}</p>
        <img src={props.flags.svg} width='50%' height='50%'/>
    </div>
}
 
export default Country;