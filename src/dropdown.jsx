import React from 'react';

class Dropdown extends React.Component {
     
    render() {
        return (
        <select style={{width: '20%', alignSelf: 'center'}} className='filter' type='text' onChange={(e)=> this.props.changes(e.target.value)}>
            <option>choose any region</option>
            <option>Americas</option>
            <option>Europe</option>
            <option>Africa</option>
            <option>Asia</option>
            <option>Oceania</option>
            <option>Antarctic</option>
        </select>) 

}
}
export default Dropdown;